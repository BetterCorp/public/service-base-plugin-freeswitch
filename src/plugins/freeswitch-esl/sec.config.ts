import { FreeSwitchESLPluginConfig } from '../../lib';

export default (): FreeSwitchESLPluginConfig => {
  return {
    server: {
      enabled: false,
      host: '127.0.0.1',
      port: 8085,
      myEvents: true
    },
    client: {
      enabled: false,
      host: "127.0.0.1",
      port: 8021,
      password: "ClueCon",
      subscribe: false,
      customHeaders: []
    }
  };
};