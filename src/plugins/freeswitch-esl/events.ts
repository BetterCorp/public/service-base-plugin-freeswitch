import { PluginFeature } from '@bettercorp/service-base/lib/ILib';
import * as ESL from 'modesl';

export class FreeSwitchEvents {
  private _FSW!: ESL.Connection;
  private _CustomHeaders: Array<string> = [];
  private features!: PluginFeature;
  public emitEvent: Function = () => { };
  private _liveBuffer = { row_count: 0, rows: [] };

  public sendSMS(to: string, toProvider: string, from: string, profile: string, body: string) {
    this._FSW.message({
      to: to + '@' + toProvider,
      from: from,
      profile: profile,
      body: body
    });
  }

  public execute(app: string, args: Array<string>, callId?: string): Promise<any> {
    const self = this;
    return new Promise((resolve: any, reject) => {
      // originate {origination_caller_id_number=[tonum]}user/[ext]@[tenant] [tonum] XML [context - tenant]
      try {
        self.features.log.info(`Execute: ${ app } [${ args.join(' ') }] ${ callId || '' }`);
        self._FSW.execute(app, args.join(' '), callId, resolve);
      } catch (err) {
        console.error(err);
        reject(err);
      }
    });
  }

  public api(command: string, args: Array<string>): Promise<any> {
    const self = this;
    return new Promise((resolve: any, reject) => {
      try {
        self.features.log.info(`API: ${ command } [${ args.join(' ') }]`);
        (self._FSW.api as any)(command, args, (res: any): void => {
          //res is an esl.Event instance
          resolve(res.getBody());
        });
      } catch (err) {
        console.error(err);
        reject(err);
      }
    });
  }

  init(features: PluginFeature, host: string, port: number, password?: string, subscribe: boolean | Array<string> = false, customHeaders?: Array<string>): Promise<void> {
    let self = this;
    self.features = features;
    return new Promise((resolve) => {
      self._CustomHeaders = customHeaders || [];
      features.log.info("Connecting as ESL Client: " + host + ':' + port);
      self._FSW = new ESL.Connection(host, port, password, () => {
        features.log.info("Connected as ESL Client?: " + host + ':' + port);
        features.log.info('Custom headers(' + self._CustomHeaders.length + '): ' + self._CustomHeaders.join(','));
        const subScribe = (): Promise<void> => {
          return new Promise((resolve2) => {
            if (subscribe !== true && typeof subscribe !== 'object') return resolve2();
            self._FSW.subscribe(typeof subscribe === 'object' ? subscribe : [
              'CHANNEL_CREATE',
              'CHANNEL_CALLSTATE',
              'CHANNEL_STATE',
              'CHANNEL_HANGUP',
              'CHANNEL_ANSWER',
              'CHANNEL_PROGRESS',
              'CHANNEL_EXECUTE',
              'CHANNEL_EXECUTE_COMPLETE',
              'CHANNEL_DESTROY',
              'DTMF',
              'MESSAGE'
            ], function () {
              //configure, and setup routes
              self._subLiveUpdates(self._liveBuffer);
              resolve2();
            });
          });
        };
        (self._FSW.api as any)('status', [], (res: any): void => {
          //res is an esl.Event instance
          features.log.info("Connected as ESL Client[CONFIRMED]: " + host + ':' + port);
          features.log.info(res.getBody());
          subScribe().then(resolve).catch(features.log.error);
        });
      });
    });
  }

  private lastMsg: number = 0;
  private _subLiveUpdates(buffer: any) {
    var self = this;

    self._FSW.on('esl::event::DTMF::*', (evt: any) => {
      var id = evt.getHeader('Unique-ID');
      let evntData: any = {
        type: 'dtmf',
        digit: evt.getHeader('DTMF-Digit'),
        duration: evt.getHeader('DTMF-Duration'),
        uuid: id,
        data: {
          fromNumber: evt.getHeader('Caller-Caller-ID-Number'),
          fromName: evt.getHeader('Caller-Caller-ID-Name'),
          fromUsername: evt.getHeader('Caller-Username'),
          fromNetNumber: evt.getHeader('Caller-Caller-ID-Number'),
          toDestination: evt.getHeader('Caller-Destination-Number'),
          body: evt.getBody()
        }
      };
      //for (let header of self._CustomHeaders)
      //  evntData.data[header] = evt.getHeader(header);
      self.emitEvent(evntData);
    });

    self._FSW.on('esl::event::MESSAGE::*', (evt: any) => {
      var n = evt.getHeader('from_user'),
        t = evt.getHeader('to_user'),
        seq = parseInt(evt.getHeader('Event-Sequence'), 10);

      //with action="fire" in the chatplan, you sometimes
      //will get the message 2 times O.o
      if (seq <= this.lastMsg) return;

      this.lastMsg = seq;

      self.emitEvent({ type: 'sms', from: n, to: t, data: evt.getBody() });
    });

    //subscribe to the live Channel events, and emit
    //each event's data to the client
    self._FSW.on('esl::event::CHANNEL_CREATE::*', (evt: any) => {
      var id = evt.getHeader('Unique-ID');

      buffer.row_count++;
      buffer.rows[id] = self._createNewChannel(evt);

      self.emitEvent({ type: 'call', fkey: 'CHANNEL_CREATE', uuid: id, data: buffer.rows[id] });
    });

    self._FSW.on('esl::event::CHANNEL_CALLSTATE::*', (evt: any) => {
      var id = evt.getHeader('Unique-ID');

      //can be called after being destroyed
      if (buffer.rows[id]) {
        self._updateCallState(buffer, evt, id);
        self.emitEvent({ type: 'call', fkey: 'CHANNEL_CALLSTATE', uuid: id, data: buffer.rows[id] });
      }
    });

    self._FSW.on('esl::event::CHANNEL_STATE::*', (evt: any) => {
      var id = evt.getHeader('Unique-ID');

      if (buffer.rows[id]) {
        self._updateState(buffer, evt, id);
        self.emitEvent({ type: 'call', fkey: 'CHANNEL_STATE', uuid: id, data: buffer.rows[id] });
      }
    });

    self._FSW.on('esl::event::CHANNEL_HANGUP::*', (evt: any) => {
      var id = evt.getHeader('Unique-ID');

      if (buffer.rows[id]) {
        self._updateState(buffer, evt, id);
        self.emitEvent({ type: 'call', fkey: 'CHANNEL_HANGUP', uuid: id, data: buffer.rows[id] });
      }
    });

    self._FSW.on('esl::event::CHANNEL_PROGRESS::*', (evt: any) => {
      var id = evt.getHeader('Unique-ID');

      if (buffer.rows[id]) {
        self._updateState(buffer, evt, id);
        self.emitEvent({ type: 'call', fkey: 'CHANNEL_PROGRESS', uuid: id, data: buffer.rows[id] });
      }
    });

    self._FSW.on('esl::event::CHANNELCHANNEL_ANSWER_STATE::*', (evt: any) => {
      var id = evt.getHeader('Unique-ID');

      if (buffer.rows[id]) {
        self._updateState(buffer, evt, id);
        self.emitEvent({ type: 'call', fkey: 'CHANNEL_ANSWER', uuid: id, data: buffer.rows[id] });
      }
    });

    self._FSW.on('esl::event::CHANNEL_EXECUTE::*', (evt: any) => {
      var id = evt.getHeader('Unique-ID');

      if (buffer.rows[id]) {
        self._updateState(buffer, evt, id);
        self.emitEvent({ type: 'call', fkey: 'CHANNEL_EXECUTE', uuid: id, data: buffer.rows[id] });
      }
    });

    self._FSW.on('esl::event::CHANNEL_EXECUTE_COMPLETE::*', (evt: any) => {
      var id = evt.getHeader('Unique-ID');

      if (buffer.rows[id]) {
        self._updateState(buffer, evt, id);
        self.emitEvent({ type: 'call', fkey: 'CHANNEL_EXECUTE_COMPLETE', uuid: id, data: buffer.rows[id] });
      }
    });

    self._FSW.on('esl::event::CHANNEL_DESTROY::*', (evt: any) => {
      var id = evt.getHeader('Unique-ID');

      buffer.row_count--;
      delete buffer.rows[id];

      self.emitEvent({ type: 'call', fkey: 'CHANNEL_DESTROY', uuid: id, destroy: true });
    });
  }

  private _updateState(buff: any, e: any, id: any) {
    buff.rows[id].state = e.getHeader('Channel-State');
    buff.rows[id].callstate = e.getHeader('Channel-Call-State');
    if (e.getHeader('Answer-State') !== undefined && e.getHeader('Answer-State') !== null)
      buff.rows[id].answerstate = e.getHeader('Answer-State');
    if (e.getHeader('Caller-Context') !== undefined && e.getHeader('Caller-Context') !== null)
      buff.rows[id].context = e.getHeader('Caller-Context');
    if (e.getHeader('Application-Response') !== undefined && e.getHeader('Application-Response') !== null)
      buff.rows[id].application_response = e.getHeader('Application-Response');
    if (e.getHeader('Application') !== undefined && e.getHeader('Application') !== null)
      buff.rows[id].application = e.getHeader('Application');
    if (e.getHeader('Application-Data') !== undefined && e.getHeader('Application-Data') !== null)
      buff.rows[id].application_data = e.getHeader('Application-Data');
    buff.rows[id].hit_dialplan = e.getHeader('Channel-HIT-Dialplan');

    for (let header of this._CustomHeaders) {
      buff.rows[id][header] = e.getHeader(header) || buff.rows[id][header] || null;
    }
  };

  private _updateCallState(buff: any, e: any, id: any) {
    buff.rows[id].read_codec = e.getHeader('Channel-Read-Codec-Name');
    buff.rows[id].read_rate = e.getHeader('Channel-Read-Codec-Rate');
    buff.rows[id].read_bit_rate = e.getHeader('Channel-Read-Codec-Bit-Rate');
    buff.rows[id].write_codec = e.getHeader('Channel-Write-Codec-Name');
    buff.rows[id].write_rate = e.getHeader('Channel-Write-Codec-Rate');
    buff.rows[id].write_bit_rate = e.getHeader('Channel-Write-Codec-Bit-Rate');
    buff.rows[id].context = e.getHeader('Caller-Context');

    this._updateState(buff, e, id);
  };

  private _createNewChannel(e: any) {
    let chanData: any = {
      //xDomainId: e.getHeader('X_DOMAIN_UUID'),
      //xDomainName: e.getHeader('X_DOMAIN_NAME'),
      uuid: e.getHeader('Unique-ID'),
      direction: e.getHeader('Call-Direction'),
      created: e.getHeader('Event-Date-Local'),
      created_epoch: Math.floor(e.getHeader('Event-Date-Timestamp') / 1E6),
      name: e.getHeader('Channel-Name'),
      state: e.getHeader('Channel-State'),
      cid_name: e.getHeader('Caller-Callee-ID-Name') || e.getHeader('Caller-Caller-ID-Name'),
      cid_num: e.getHeader('Caller-Callee-ID-Number') || e.getHeader('Caller-Caller-ID-Number'),
      ip_addr: '',
      dest: e.getHeader('Caller-Destination-Number'),
      application: '', //in CHANNEL_EXECUTE
      application_data: '', //in CHANNEL_EXECUTE
      dialplan: null, //Not in messages
      context: e.getHeader('Caller-Context'),
      read_codec: '', //in CHANNEL_CALLSTATE
      read_rate: '', //in CHANNEL_CALLSTATE
      read_bit_rate: '', //in CHANNEL_CALLSTATE
      write_codec: '', //in CHANNEL_CALLSTATE
      write_rate: '', //in CHANNEL_CALLSTATE
      write_bit_rate: '', //in CHANNEL_CALLSTATE
      secure: null, //Not in messages
      hostname: e.getHeader('FreeSWITCH-Hostname'),
      switchname: e.getHeader('FreeSWITCH-Switchname'),
      presence_id: null, //Not in messages
      presence_data: null, //Not in messages
      callstate: '', //in CHANNEL_CALLSTATE
      callee_name: e.getHeader('Caller-Callee-ID-Name'),
      callee_num: e.getHeader('Caller-Callee-ID-Number'),
      callee_direction: null, //Not in messages
      call_uuid: e.getHeader('Channel-Call-UUID'),
      sent_callee_name: null, //Not in messages
      sent_callee_num: null //Not in messages
    };

    for (let header of this._CustomHeaders)
      chanData[header] = e.getHeader(header) || null;

    return chanData;
  };
}