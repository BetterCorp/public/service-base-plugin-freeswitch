import { CPlugin } from '@bettercorp/service-base/lib/ILib';
import { FreeSwitchDelCDRRecording, FreeSwitchGetCDRRecording, FreeSwitchGetCDRRecordingPostRequest, FreeSwitchGetCDRRecordingResponse, FreeSwitchPluginConfig, FreeSwitchPluginEvents } from '../../lib';
import * as crypto from 'crypto';
import { Tools } from '@bettercorp/tools/lib/Tools';
import * as FS from 'fs';
import { Response as ExpressResponse, Request as ExpressRequest } from 'express';
import * as EXPRESS from 'express';
import * as FreeSwitch from '../../freeswitch';
import { express } from '@bettercorp/service-base-plugin-web-server/lib/plugins/express/express';

export class Plugin extends CPlugin<FreeSwitchPluginConfig> {
  private express!: express;
  private getFileHash(filePath: string): Promise<string> {
    return new Promise((resolve, reject) => {
      if (!FS.existsSync(filePath)) return reject(`${ filePath } does not exist!`);

      try {
        // https://gist.github.com/F1LT3R/2e4347a6609c3d0105afce68cd101561
        const hash = crypto.createHash('sha1');
        const rs = FS.createReadStream(filePath);
        rs.on('error', reject);
        rs.on('data', (chunk: any) => hash.update(chunk));
        rs.on('end', () => resolve(hash.digest('hex')));
      } catch (exzc) {
        reject(exzc);
      }
    });
  }
  init(): Promise<void> {
    const self = this;
    return new Promise((resolve) => {
      self.express = new express(self);
      self.log.info('ACAO FOR EXPRESS');
      self.express.use(async (req: any, res: any, next: any) => {
        self.log.debug(`REQ[${ req.method }] ${ req.path } (${ JSON.stringify(req.query) })`);
        res.setHeader('Access-Control-Allow-Headers', '');
        res.setHeader('Access-Control-Allow-Origin', '');
        res.setHeader('Access-Control-Allow-Methods', '');
        next();
      });
      self.log.info('USE JSON FOR EXPRESS');
      self.express.use(EXPRESS.json({ limit: '5mb' }));
      self.express.post('/CDR', async (req: ExpressRequest, res: ExpressResponse) => {
        try {
          let id = req.query.uuid;
          let cdr = req.body as FreeSwitch.CDR;
          self.emitEvent(null, FreeSwitchPluginEvents.OnCDREvent, {
            id: id,
            cdr: cdr
          });
          res.sendStatus(202);
        } catch (exc) {
          self.log.error(exc);
          res.send(500);
        }
      });
      self.express.post('/Recording/Download', async (req: ExpressRequest, res: ExpressResponse) => {
        try {
          let data = req.body as any as FreeSwitchGetCDRRecordingPostRequest;
          if (Tools.isNullOrUndefined(data)) return res.status(400).send('INVALID DATA');
          if (Tools.isNullOrUndefined(data.uuid)) return res.status(400).send('INVALID UUID');
          if (Tools.isNullOrUndefined(data.domain)) return res.status(400).send('INVALID DOMAIN');
          if (Tools.isNullOrUndefined(data.year)) return res.status(400).send('INVALID YEAR');
          if (Tools.isNullOrUndefined(data.month)) return res.status(400).send('INVALID MONTH');
          if (Tools.isNullOrUndefined(data.day)) return res.status(400).send('INVALID DAY');
          if (Tools.isNullOrUndefined(data.hash)) return res.status(400).send('INVALID FILE');

          let recordingPath = `${ self.getPluginConfig().recordingsPath }`;
          recordingPath = recordingPath.replace('{DOMAIN}', data.domain);
          recordingPath = recordingPath.replace('{YEAR}', data.year);
          recordingPath = recordingPath.replace('{MONTH}', data.month);
          recordingPath = recordingPath.replace('{DAY}', data.day);
          recordingPath = recordingPath.replace('{UUID}', data.uuid);

          self.log.debug(`CHECK Recording: ${ recordingPath }`);
          if (!FS.existsSync(recordingPath)) {
            return res.sendStatus(404);
          }
          let fileHash = await self.getFileHash(recordingPath);
          if (fileHash !== data.hash) {
            return res.status(400).send('INVALID RECORDING');
          }
          res.setHeader("content-type", "application/octet-stream");
          return FS.createReadStream(recordingPath).pipe(res);
        } catch (exc) {
          self.log.error(exc);
          return res.sendStatus(500);
        }
      });
      self.onReturnableEvent<FreeSwitchGetCDRRecording>(null, FreeSwitchPluginEvents.GetCDRCallRecording, async (resolve, reject, data) => {
        if (Tools.isNullOrUndefined(data)) throw 'NO DATA';
        if (Tools.isNullOrUndefined(data.uuid)) throw 'NO DATA: uuid';
        if (Tools.isNullOrUndefined(data.domain)) throw 'NO DATA: domain';
        if (Tools.isNullOrUndefined(data.year)) throw 'NO DATA: year';
        if (Tools.isNullOrUndefined(data.month)) throw 'NO DATA: month';
        if (Tools.isNullOrUndefined(data.day)) throw 'NO DATA: day';

        let recordingPath = `${ self.getPluginConfig().recordingsPath }`;
        recordingPath = recordingPath.replace('{DOMAIN}', data.domain);
        recordingPath = recordingPath.replace('{YEAR}', data.year);
        recordingPath = recordingPath.replace('{MONTH}', data.month);
        recordingPath = recordingPath.replace('{DAY}', data.day);
        recordingPath = recordingPath.replace('{UUID}', data.uuid);

        self.log.debug(`CHECK Recording: ${ recordingPath }`);
        if (!FS.existsSync(recordingPath)) {
          return reject(`Can't find recording!`);
        }
        resolve({
          url: `${ self.getPluginConfig().externalHost }/Recording/Download`,
          hash: await self.getFileHash(recordingPath)
        } as FreeSwitchGetCDRRecordingResponse);
      });
      self.onReturnableEvent<FreeSwitchDelCDRRecording>(null, FreeSwitchPluginEvents.DeleteCDRCallRecording, async (resolve, reject, data) => {
        if (Tools.isNullOrUndefined(data)) throw 'NO DATA';
        if (Tools.isNullOrUndefined(data.uuid)) throw 'NO DATA: uuid';
        if (Tools.isNullOrUndefined(data.domain)) throw 'NO DATA: domain';
        if (Tools.isNullOrUndefined(data.year)) throw 'NO DATA: year';
        if (Tools.isNullOrUndefined(data.month)) throw 'NO DATA: month';
        if (Tools.isNullOrUndefined(data.day)) throw 'NO DATA: day';

        let recordingPath = `${ self.getPluginConfig().recordingsPath }`;
        recordingPath = recordingPath.replace('{DOMAIN}', data.domain);
        recordingPath = recordingPath.replace('{YEAR}', data.year);
        recordingPath = recordingPath.replace('{MONTH}', data.month);
        recordingPath = recordingPath.replace('{DAY}', data.day);
        recordingPath = recordingPath.replace('{UUID}', data.uuid);

        self.log.debug(`CHECK Recording: ${ recordingPath }`);
        if (!FS.existsSync(recordingPath)) {
          return reject(`Can't find recording!`);
        }
        FS.unlinkSync(recordingPath);
        resolve();
      });
      self.log.debug('FreeSwitch API READY');
      resolve();
    });
  }
}