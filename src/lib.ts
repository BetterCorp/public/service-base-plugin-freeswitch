export enum FreeSwitchPluginEvents {
  OnCDREvent = "on-cdr-event",
  GetCDRCallRecording = "get-call-recording",
  DeleteCDRCallRecording = "del-call-recording",
}

export interface FreeSwitchPluginConfig {
  externalHost: string;
  recordingsPath: string;
}
export interface FreeSwitchGetCDRRecording {
  uuid: string;
  domain: string;
  year: string;
  month: string;
  day: string;
}
export interface FreeSwitchDelCDRRecording extends FreeSwitchGetCDRRecording {
}
export interface FreeSwitchGetCDRRecordingPostRequest extends FreeSwitchGetCDRRecording {
  hash: string;
}
export interface FreeSwitchGetCDRRecordingResponse {
  url: string;
  hash: string;
}


export enum FreeSwitchPluginESLEvents {
  SendSMS = "send-sms",
  OnESLEvent = "on-event",
  Execute = "execute",
  OnESLServerEvent = "on-smart-server-event"
}
export enum FreeSwitchESLServerEventNames {
  answer = "answer",
  hangUp = "hangup",
  playback = "playback",
  ivr = "ivr",
  transfer = "transfer",
  //bridge = "bridge",
  //echo = "echo",
  continue = "continue",
  custom = "custom",
}
export interface FreeSwitchESLServerEventTrigger {
  lastEvent: FreeSwitchESLServerEvent | null;
  callInfo: any;
  id: string;
}
export interface FreeSwitchESLServerEventHangup {
  reason: string;
}
export interface FreeSwitchESLServerEventPlayback {
  terminators?: string; // none
  file: string; // full path to file
  fileIsRemote: Boolean;
}
export interface FreeSwitchESLServerEventIVR {
  minDigits: number;
  maxDigits: number;
  retries: number;
  timeout: number;
  digitTimeout: number;
  terminator: string;
  playFile: string; // full path to file
  playFileIsRemote: Boolean;
  invalidFile: string; // full path to file
  invalidFileIsRemote: Boolean;

  // response
  responseFailure?: Boolean
  responseDigits?: string
}
export enum FreeSwitchESLServerEventTransferType {
  bleg = "-bleg",
  both = "-both"
}
export interface FreeSwitchESLServerEventTransfer {
  destination: string;
  domainName: string;
  dialplan: string;
  type: FreeSwitchESLServerEventTransferType;
}
export enum FreeSwitchESLServerEventCustomFunctions {
  api,
  getInfo,
  originate,
  bgapi,
  execute
}
export interface FreeSwitchESLServerEventCustom {
  function: FreeSwitchESLServerEventCustomFunctions;
  app?: string;
  command?: string;
  options?: any;
  value?: any;
  args?: Array<string>;
}
export interface FreeSwitchESLServerEvent {
  localKey?: string;
  name: FreeSwitchESLServerEventNames;
  hangup?: FreeSwitchESLServerEventHangup;
  playback?: FreeSwitchESLServerEventPlayback;
  ivr?: FreeSwitchESLServerEventIVR;
  transfer?: FreeSwitchESLServerEventTransfer;
  custom?: FreeSwitchESLServerEventCustom;

  response?: any;
}
export enum FreeSwitchESLExecuteType {
  execute,
  api
}
export interface FreeSwitchESLExecute {
  type: FreeSwitchESLExecuteType;
  appOrCommand: string;
  args: Array<string>;
  callId?: string;
}
export interface FreeSwitchESLSMS {
  to: string;
  toProvider: string;
  from: string;
  profile: string;
  body: string
}

export interface FreeSwitchESLPluginConfig {
  server: FreeSwitchESLPluginConfigServer;
  client: FreeSwitchESLPluginConfigClient;
}
export interface FreeSwitchESLPluginConfigServer {
  enabled: Boolean;
  host: string;
  port: number;
  myEvents: boolean;
}
export interface FreeSwitchESLPluginConfigClient {
  enabled: Boolean;
  host: string;
  port: number;
  password?: string;
  subscribe?: boolean | Array<string>
  customHeaders?: Array<string>
}