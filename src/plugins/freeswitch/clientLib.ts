import * as FS from 'fs';
import stream = require('stream');
import axios from 'axios';
import { FreeSwitchPluginEvents, FreeSwitchGetCDRRecording, FreeSwitchGetCDRRecordingResponse, FreeSwitchGetCDRRecordingPostRequest } from '../../lib';
import { Plugin } from './plugin';

export class FreeSwitch {
  public GetCDRCallRecording(uSelf: Plugin, destinationPluginName: string, domain: string, uuid: string, year: string, month: string, day: string, destination: string | stream.Writable): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        let writeStream: stream.Writable;
        if (typeof destination === 'string') {
          // write to file:
          writeStream = FS.createWriteStream(destination);
        } else {
          writeStream = destination;
        }

        let requestInfo = await uSelf.emitEventAndReturn<FreeSwitchGetCDRRecording, FreeSwitchGetCDRRecordingResponse>(destinationPluginName, FreeSwitchPluginEvents.GetCDRCallRecording, {
          domain,
          uuid,
          year,
          month,
          day
        });

        let postData: FreeSwitchGetCDRRecordingPostRequest = {
          hash: requestInfo.hash,
          domain,
          uuid,
          year,
          month,
          day
        };

        const response = await axios({
          url: requestInfo.url,
          method: 'POST',
          responseType: 'stream',
          data: postData
        });

        response.data.pipe(writeStream);

        writeStream.on('finish', resolve);
        writeStream.on('error', reject);
      } catch (exc) {
        reject(exc);
      }
    });
  }
}